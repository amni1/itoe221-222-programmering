# Minimumskrav for gyldigt for loop:

for i in range(100):
    pass

# Definere antal gange det skal loope, f.eks. i form af range.
# Et variabel at loope på, så vi ved hvor mange gange der er loopet
# Kolon før kommandoer

liste = [12, 14, 16, 16, 18, 10]
for item in liste:
    pass

# Hvilken Git commando til at hente ændringer i repository?
# git pull

# Hvad gør git push efter et commit? 
# Det "skubber" ændringerne ind i repository

# Kan en funktion returnerer flere variable?

def check_values(temp, humidity):
    if temp < 0 or temp > 50:
        print("Temperaturen på ", temp,  "er udenfor det tilladte!")
    elif temp >= 0 and temp <= 50:
        print("Temperatur er: ", temp)
    if humidity < 20 or humidity > 90:
        print("Luftfugtigheden på ", humidity, "er udenfor det tilladte!")
    elif humidity >= 20 and humidity <= 90:
        print("Luftfugtigheden er: ", humidity) 
    return [temp, humidity]

værdier = check_values(70, 50)
# print(værdier)
'''
Ja det kan man! Ved at kommaseparere return variable. Alternativt
kan man også gemme return variable i en datastruktur,
som f.eks. liste eller tuple.
'''

# Vilkårligt antal inputs

def temperatur_multiinput_check(*temps):
    error_count = 0
    for temp in temps:
        if temp < 0 or temp > 50:
            print("Temperaturen på ", temp,  "er udenfor det tilladte!")
            error_count += 1
        elif temp >= 0 and temp <= 50:
            print("Temperatur er: ", temp)
    print("Antal inputs uden for rækkevidde: ", error_count)

temperatur_multiinput_check(40, 60, 70, 80)


'''
Vi kan godtage flere inputs ved at putte * foran inputværdien.
Så bliver inputs til en tuple, og derfor bliver vi nødt
til at loope over inputs nu. 
'''
'''
Vi kan tjekke hvor mange af inputs der var uden for range, 
ved at have et tællevariabel som starter i 0, og så tæller vi
hver gang en condition er opfyldt. 
'''