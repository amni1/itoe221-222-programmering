# 1
tal1 = "19"
tal2 = "14"
print(int(tal1) + int(tal2))

# 2
liste = [120, 600, 0.5, 0.2]
print((liste[0] + liste[1]) * liste[2])

# 3
liste1 = ["er", "kr", "is", "tid", "e"]
print(liste1[1] + liste1[2] + liste1[4] +  liste1[3] + liste1[0])

# 4
liste2 = ["m", 1, "n", 4]
print(liste2[0] + str(liste2[1]) + liste2[2] + str(liste2[1]) + liste2[0] + str(liste2[3]) + str(liste2[1]))

# 5
telefonbog = {    
    "Alice": "2341",
    "Beth": "9102",
    "Cecil": "3158",
    "Hans":  "2345"
    }
telefonbog["Hans"]

# 6
x = 25
y = 16
def adder(i, j):
    result = i + j
    print(result)

adder(x, y)

# 7
omkostninger = [1000, 15152, 354747, 4732153, 15612, 34346, 4212, 1515, 8453]
resultat = 0
for pris in omkostninger:
    resultat += pris
# Alternativt: sum(omkostninger)
print(resultat)

# 8
def string_checker(some_input):
    if type(some_input) == type(""):
        print("Dette er en string!")
    else: 
        print('Dette er ikke en string.')
string_checker(283746)


# 9 - Hotdog not hotdog

def hnoth(input_string):
    if input_string.lower() == "hotdog":
        print('This is a hotdog')
    else:
        print('Not hotdog.')
hnoth("Hotdog")

print("RTrioetoWEWE".upper())

# 10
string1 = "Hvordan"
string2 = "tæller"
string3 = "vi"
string4 = "længden"
string5 = "af flere strings?"
def multi_længde(*ord):
    counter = 0
    for word in ord:
        counter += len(word)
    print(counter)

multi_længde(string1, string2, string3, string4, string5)

print(len("harald "))

# 11
from time import sleep
def logging(worker_id, status):
    minutes = 0
    try:
        while True:
            sleep(0.1) # Vi lader som om det er et minut :)
            minutes +=1
            print(minutes)
    except KeyboardInterrupt:
        pass
    return minutes

print(logging(2, "working"))

'''minutter = logging(2, "working")
print(minutter)'''

# 12
# DeviceFactory(device_id, hovedfunktion, ekstra)

class DeviceFactory:
    def __init__(self, device_id, hoved_funktion, ekstra):
        self.device_id = device_id
        self.hoved_funktion = hoved_funktion
        self.ekstra = ekstra
        self.active = True
    def status_check(self):
        print(self.active)
        if self.active == True:
            self.active = False
        else:
            self.active = True

device1 = DeviceFactory("1231", "LDR", "Temperatur")
device2 = DeviceFactory("1232", "Co2", "Luftfugtighed")
# Metodekald
device1.status_check()
device2.status_check()
