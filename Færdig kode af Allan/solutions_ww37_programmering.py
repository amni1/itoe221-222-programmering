import pandas as pd

''' 
Data hentet fra Kaggle - https://www.kaggle.com/datasets/tarundalal/animated-movies-imdb
Det er data over top 85 animationsfilm på IMDB.
'''
table = pd.read_csv("https://raw.githubusercontent.com/AllanMisasa/UCL-Code/main/Python/TopAnimatedImDb.csv")
# print(table.head())

'''
Lister ud fra forskellige datapunkter. 
Titel - Titel på film
Vurdering - Alle ratings på IMDB
Metascore - Metascore - samlet score fra Metacritic
År - Årstal for udgivelse af filmen
Antal_vurderinger - Antal ratings på IMDB
Instruktører - Hvem der var instruktør på filmen
'''
titel = list(table['Title'])
vurdering = list(table['Rating'])
metascore = list(table['Metascore'])
år = list(table['Year'])
antal_vurderinger = list(table['Votes'])
instruktører = list(table['Director'])

# Opgave 1 - task 5

count = 0
for score in metascore:
    if score > 80:
        count += 1 # count = count + 1

# Opgave 2 - task 1

# print(len(titel))

# Opgave 2 - task 2

print(titel) 
for title in titel:
    if title == "Toy Story":
        print("Min yndlingsfilm er i listen!") 

print("Toy Story" in titel)

# Opgace 2 - task 3

print("Wes Anderson" in instruktører, "Wes Anderson er i listen af instruktører.")

# Opgave 2 - task 4
count1 = []
for instruktør in instruktører:
    if instruktør == "Hayao Miyazaki":
        count1.append(0)
print(len(count1))

# Opgave 2 - task 5
count2 = 0
for tal in vurdering:
    if tal > 8:
        count2 +=1
print("Der er ", count2, "film rated over 8.0 på IMDB" )    

# Opgave 2 - task 6

indeks = 0
årstal = 3000
while indeks < len(år):
    if år[indeks] < årstal:
        årstal = år[indeks]
    indeks +=1
print(årstal)
