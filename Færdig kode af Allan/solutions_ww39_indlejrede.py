from machine import Pin, ADC, PWM
import time

frequency = 5000
led = PWM(Pin(2), frequency)
ldr_pin = 13
ldr = ADC(Pin(ldr_pin))
ldr.atten(ADC.ATTN_11DB)
ldr.width(ADC.WIDTH_10BIT)

while True:
    print(ldr.read())
    time.sleep(0.5)