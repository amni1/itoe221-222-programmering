'''
1. Hvordan refererer man i klassen til et objekts attributer?
Med keyword `self`
2. Hvordan initialiserer man et objekt ud fra en klasse? 
Klassenavn(variable)
3. Hvordan sørger vi for at en metode har adgang til klassens objekt? 
Ved at give den `self` som argument.
4. Kan man definere variable i `__init__` der ikke tilhører klassen selv? *Hint: Test det i VSCode.*
Ja det kan man, uden brug af self tilhører de ikke objektet.
5. Hvad vil du bruge objektorienteret programmering til i fremtiden? Skriv minimum 3 ting ned.
Åbent.
'''

# Klasse 1 - OptiClk

'''
import time

start_tid = time.process_time() 

n = 0
for i in range(1000000):
    n += i

time.sleep(3)
print("n er: ", n)

slut_tid = time.process_time()

køretid = slut_tid - start_tid
print('CPUens køretid er: ', køretid, 'sekunder')
'''

import time

class OptiClk:
    def __init__(self, function):
        self.function = function
    def measure(self):
        start_tid = time.process_time()
        self.function()
        time.sleep(2)
        slut_tid = time.process_time()
        køretid = slut_tid - start_tid
        print(print('CPUens køretid er: ', køretid, 'sekunder'))
        return køretid

def adder():
    n = 0
    for i in range(100000000):
        n += i

measurer = OptiClk(adder)
measurer.measure()

# Light
'''
from machine import Pin, PWM
from time import sleep

class LTBL:
    def __init__(self, pin, led_type):
        self.pin = pin
        self.led_type = led_type
        if self.led_type == 'ToggleLED': # LED mode fra input
            self.led = Pin(self.pin, Pin.OUT)
            self.binary_led()
        elif self.led_type =='StepperLED':
            self.led = PWM(self.pin)
            self.stepper_led()

    def binary_led(self):
        while True:
            self.led.toggle()
            sleep(5)
    def stepper_led(self):
        frequency = 5000
        start = 0
        self.led.freq(frequency)
        steps = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        for step in steps:
            self.led.duty(step)
            sleep(0.5)

'''
# Wololo

class wololo:
    def __init__(self, value):
        self.type = type(value)
        self.value = value
    def ftoc(self):
        celsius = (self.value - 32) * 5/9
        print('Temperaturen i celsius er ', celsius)
        return celsius

objekt = wololo(80)
objekt.ftoc()
