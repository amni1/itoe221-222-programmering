from machine import Pin
from time import sleep
import dht

sensor = dht.DHT11(Pin(14))

def check_values(temp, humidity):
    if temp < 0 or temp > 50:
        print("Temperaturen på ", temp,  "er udenfor det tilladte!")
    elif temp >= 0 and temp <= 50:
        print("Temperatur er: ", temp)
    if humidity < 20 or humidity > 90:
        print("Luftfugtigheden på ", humidity, "er udenfor det tilladte!")
    elif humidity >= 20 and humidity <= 90:
        print("Luftfugt-igheden er: ", humidity) 

while True:
    sensor.measure()
    temperatur = sensor.temperature()
    luftfugtighed = sensor.humidity()
    check_values(temperatur, luftfugtighed)
    sleep(1)
